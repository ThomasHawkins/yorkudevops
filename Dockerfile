FROM node

COPY . ./

RUN npm i

EXPOSE 8080

CMD ./wait-for-it.sh db:3306 -- npm run start