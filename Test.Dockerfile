FROM node

COPY . ./

RUN npm i

EXPOSE 3000

CMD ./wait-for-it.sh -t 0 db:3306 -- npm run test
