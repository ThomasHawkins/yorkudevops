# Project Frontend Client

This is the frontend client utilizing React to consume an API.

## Overview
This project requires the setting of two variables:
- `REACT_APP_API` - for including the API base URL
- `REACT_APP_RELEASE` - used for simple text replacement for title, e.g. value of variable: `DEV` will resullt in the page title of the HTML to have `Address Book: DEV`

## Instructions

To start a local dev environment: `npm run start`, however as noted above, for the project to work properly, environment variables must be set.

For MacOS: `REACT_APP_API=http://path/to/api REACT_APP_RELEASE=Random npm start`

For Windows (command line): `set "REACT_APP_API=http://path/to/api &  REACT_APP_RELEASE=Random" && npm start`
For more information on React & Environment Variables, see [documentation](https://facebook.github.io/create-react-app/docs/adding-custom-environment-variables#adding-temporary-environment-variables-in-your-shell)

*Note:* For optimal results, when uploading to a live environment, use the `npm build` rather than `npm start` command, and upload the contents found within the `build` folder.


Cloud Deployment steps:

1) create project
2) create mySQL instance
3) create database
4) enable cloud sql API
5) create .yaml file and configure environment variables
6) ensure node app uses port 8080 and the docker file exposes it


CI/CD steps:

1) set up .gitlab-ci.yml file with test and deploy stages and jobs
2) enable App engine admine API through the google cloud console
3) create a servcie account for gitlab CD via google cloud console
4) navigate to the gitlab project repository
5) under settings > CI/CD